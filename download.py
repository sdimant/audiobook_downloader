import os
import urllib.request
import json

base_url = 'https://files.audiobookss.com/audio/'
js_str = '''[
    {
        "track": 1,
        "name": "welcome",
        "chapter_link_dropbox": "https://file.tokybook.com/upload/welcome-you-to-sidabook.mp3",
        "duration": "8",
        "chapter_id": "0",
        "post_id": "0"
    },
    {
        "track": 2,
        "name": "Warbreaker (Unabridged) - 001",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - 001-670333328.mp3",
        "duration": "193.48",
        "chapter_id": "670333328",
        "post_id": "9813"
    }, {
        "track": 3,
        "name": "Warbreaker (Unabridged) - Chapter 2",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 2-670333322.mp3",
        "duration": "1448.76",
        "chapter_id": "670333322",
        "post_id": "9813"
    }, {
        "track": 4,
        "name": "Warbreaker (Unabridged) - Chapter 3",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 3-670333304.mp3",
        "duration": "1616.56",
        "chapter_id": "670333304",
        "post_id": "9813"
    }, {
        "track": 5,
        "name": "Warbreaker (Unabridged) - Chapter 4",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 4-670333298.mp3",
        "duration": "1156.13",
        "chapter_id": "670333298",
        "post_id": "9813"
    }, {
        "track": 6,
        "name": "Warbreaker (Unabridged) - Chapter 5",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 5-670333289.mp3",
        "duration": "1615.28",
        "chapter_id": "670333289",
        "post_id": "9813"
    }, {
        "track": 7,
        "name": "Warbreaker (Unabridged) - Chapter 6",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 6-670333286.mp3",
        "duration": "835.42",
        "chapter_id": "670333286",
        "post_id": "9813"
    }, {
        "track": 8,
        "name": "Warbreaker (Unabridged) - Chapter 7",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 7-670333277.mp3",
        "duration": "1570.42",
        "chapter_id": "670333277",
        "post_id": "9813"
    }, {
        "track": 9,
        "name": "Warbreaker (Unabridged) - Chapter 8",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 8-670333268.mp3",
        "duration": "1808.99",
        "chapter_id": "670333268",
        "post_id": "9813"
    }, {
        "track": 10,
        "name": "Warbreaker (Unabridged) - Chapter 9",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 9-670333253.mp3",
        "duration": "1683.83",
        "chapter_id": "670333253",
        "post_id": "9813"
    }, {
        "track": 11,
        "name": "Warbreaker (Unabridged) - Chapter 10",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 10-670333250.mp3",
        "duration": "1272.52",
        "chapter_id": "670333250",
        "post_id": "9813"
    }, {
        "track": 12,
        "name": "Warbreaker (Unabridged) - Chapter 11",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 11-670333241.mp3",
        "duration": "1430.99",
        "chapter_id": "670333241",
        "post_id": "9813"
    }, {
        "track": 13,
        "name": "Warbreaker (Unabridged) - Chapter 12",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 12-670333232.mp3",
        "duration": "1223.61",
        "chapter_id": "670333232",
        "post_id": "9813"
    }, {
        "track": 14,
        "name": "Warbreaker (Unabridged) - Chapter 13",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 13-670333223.mp3",
        "duration": "1204.44",
        "chapter_id": "670333223",
        "post_id": "9813"
    }, {
        "track": 15,
        "name": "Warbreaker (Unabridged) - Chapter 14",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 14-670333214.mp3",
        "duration": "1465.06",
        "chapter_id": "670333214",
        "post_id": "9813"
    }, {
        "track": 16,
        "name": "Warbreaker (Unabridged) - Chapter 15",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 15-670333193.mp3",
        "duration": "1393.27",
        "chapter_id": "670333193",
        "post_id": "9813"
    }, {
        "track": 17,
        "name": "Warbreaker (Unabridged) - Chapter 16",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 16-670333190.mp3",
        "duration": "1932.07",
        "chapter_id": "670333190",
        "post_id": "9813"
    }, {
        "track": 18,
        "name": "Warbreaker (Unabridged) - Chapter 17",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 17-670333181.mp3",
        "duration": "1164.62",
        "chapter_id": "670333181",
        "post_id": "9813"
    }, {
        "track": 19,
        "name": "Warbreaker (Unabridged) - Chapter 18",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 18-670333169.mp3",
        "duration": "1581.73",
        "chapter_id": "670333169",
        "post_id": "9813"
    }, {
        "track": 20,
        "name": "Warbreaker (Unabridged) - Chapter 19",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 19-670333163.mp3",
        "duration": "1284.25",
        "chapter_id": "670333163",
        "post_id": "9813"
    }, {
        "track": 21,
        "name": "Warbreaker (Unabridged) - Chapter 20",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 20-670333157.mp3",
        "duration": "1230.4",
        "chapter_id": "670333157",
        "post_id": "9813"
    }, {
        "track": 22,
        "name": "Warbreaker (Unabridged) - Chapter 21",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 21-670333151.mp3",
        "duration": "1655.25",
        "chapter_id": "670333151",
        "post_id": "9813"
    }, {
        "track": 23,
        "name": "Warbreaker (Unabridged) - Chapter 22",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 22-670333145.mp3",
        "duration": "905.096",
        "chapter_id": "670333145",
        "post_id": "9813"
    }, {
        "track": 24,
        "name": "Warbreaker (Unabridged) - Chapter 23",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 23-670333142.mp3",
        "duration": "1036.84",
        "chapter_id": "670333142",
        "post_id": "9813"
    }, {
        "track": 25,
        "name": "Warbreaker (Unabridged) - Chapter 24",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 24-670333130.mp3",
        "duration": "4036.5",
        "chapter_id": "670333130",
        "post_id": "9813"
    }, {
        "track": 26,
        "name": "Warbreaker (Unabridged) - Chapter 25",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 25-670333124.mp3",
        "duration": "1719.65",
        "chapter_id": "670333124",
        "post_id": "9813"
    }, {
        "track": 27,
        "name": "Warbreaker (Unabridged) - Chapter 26",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 26-670333115.mp3",
        "duration": "1396.71",
        "chapter_id": "670333115",
        "post_id": "9813"
    }, {
        "track": 28,
        "name": "Warbreaker (Unabridged) - Chapter 27",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 27-670333106.mp3",
        "duration": "1470.91",
        "chapter_id": "670333106",
        "post_id": "9813"
    }, {
        "track": 29,
        "name": "Warbreaker (Unabridged) - Chapter 28",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 28-670333100.mp3",
        "duration": "2432.54",
        "chapter_id": "670333100",
        "post_id": "9813"
    }, {
        "track": 30,
        "name": "Warbreaker (Unabridged) - Chapter 29",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 29-670333094.mp3",
        "duration": "1947.85",
        "chapter_id": "670333094",
        "post_id": "9813"
    }, {
        "track": 31,
        "name": "Warbreaker (Unabridged) - Chapter 30",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 30-670333085.mp3",
        "duration": "1713.17",
        "chapter_id": "670333085",
        "post_id": "9813"
    }, {
        "track": 32,
        "name": "Warbreaker (Unabridged) - Chapter 31",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 31-670333076.mp3",
        "duration": "1743.63",
        "chapter_id": "670333076",
        "post_id": "9813"
    }, {
        "track": 33,
        "name": "Warbreaker (Unabridged) - Chapter 32",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 32-670333073.mp3",
        "duration": "1659.19",
        "chapter_id": "670333073",
        "post_id": "9813"
    }, {
        "track": 34,
        "name": "Warbreaker (Unabridged) - Chapter 33",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 33-670333055.mp3",
        "duration": "1816.88",
        "chapter_id": "670333055",
        "post_id": "9813"
    }, {
        "track": 35,
        "name": "Warbreaker (Unabridged) - Chapter 34",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 34-670333049.mp3",
        "duration": "2328.62",
        "chapter_id": "670333049",
        "post_id": "9813"
    }, {
        "track": 36,
        "name": "Warbreaker (Unabridged) - Chapter 35",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 35-670333040.mp3",
        "duration": "1360.11",
        "chapter_id": "670333040",
        "post_id": "9813"
    }, {
        "track": 37,
        "name": "Warbreaker (Unabridged) - Chapter 36",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 36-670333031.mp3",
        "duration": "1678.45",
        "chapter_id": "670333031",
        "post_id": "9813"
    }, {
        "track": 38,
        "name": "Warbreaker (Unabridged) - Chapter 37",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 37-670333028.mp3",
        "duration": "1961.41",
        "chapter_id": "670333028",
        "post_id": "9813"
    }, {
        "track": 39,
        "name": "Warbreaker (Unabridged) - Chapter 38",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 38-670333013.mp3",
        "duration": "1526.79",
        "chapter_id": "670333013",
        "post_id": "9813"
    }, {
        "track": 40,
        "name": "Warbreaker (Unabridged) - Chapter 39",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 39-670332998.mp3",
        "duration": "713.758",
        "chapter_id": "670332998",
        "post_id": "9813"
    }, {
        "track": 41,
        "name": "Warbreaker (Unabridged) - Chapter 40",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 40-670332992.mp3",
        "duration": "1175.62",
        "chapter_id": "670332992",
        "post_id": "9813"
    }, {
        "track": 42,
        "name": "Warbreaker (Unabridged) - Chapter 41",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 41-670332989.mp3",
        "duration": "612.131",
        "chapter_id": "670332989",
        "post_id": "9813"
    }, {
        "track": 43,
        "name": "Warbreaker (Unabridged) - Chapter 42",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 42-670332977.mp3",
        "duration": "1105.27",
        "chapter_id": "670332977",
        "post_id": "9813"
    }, {
        "track": 44,
        "name": "Warbreaker (Unabridged) - Chapter 43",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 43-670332971.mp3",
        "duration": "785.079",
        "chapter_id": "670332971",
        "post_id": "9813"
    }, {
        "track": 45,
        "name": "Warbreaker (Unabridged) - Chapter 44",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 44-670332962.mp3",
        "duration": "1037.03",
        "chapter_id": "670332962",
        "post_id": "9813"
    }, {
        "track": 46,
        "name": "Warbreaker (Unabridged) - Chapter 45",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 45-670332950.mp3",
        "duration": "2454.02",
        "chapter_id": "670332950",
        "post_id": "9813"
    }, {
        "track": 47,
        "name": "Warbreaker (Unabridged) - Chapter 46",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 46-670332947.mp3",
        "duration": "1142.52",
        "chapter_id": "670332947",
        "post_id": "9813"
    }, {
        "track": 48,
        "name": "Warbreaker (Unabridged) - Chapter 47",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 47-670332938.mp3",
        "duration": "23.486",
        "chapter_id": "670332938",
        "post_id": "9813"
    }, {
        "track": 49,
        "name": "Warbreaker (Unabridged) - Chapter 48",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 48-670332932.mp3",
        "duration": "1884",
        "chapter_id": "670332932",
        "post_id": "9813"
    }, {
        "track": 50,
        "name": "Warbreaker (Unabridged) - Chapter 49",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 49-670332923.mp3",
        "duration": "1014.56",
        "chapter_id": "670332923",
        "post_id": "9813"
    }, {
        "track": 51,
        "name": "Warbreaker (Unabridged) - Chapter 50",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 50-670332920.mp3",
        "duration": "700.408",
        "chapter_id": "670332920",
        "post_id": "9813"
    }, {
        "track": 52,
        "name": "Warbreaker (Unabridged) - Chapter 51",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 51-670332917.mp3",
        "duration": "2458.93",
        "chapter_id": "670332917",
        "post_id": "9813"
    }, {
        "track": 53,
        "name": "Warbreaker (Unabridged) - Chapter 52",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 52-670332911.mp3",
        "duration": "1953.75",
        "chapter_id": "670332911",
        "post_id": "9813"
    }, {
        "track": 54,
        "name": "Warbreaker (Unabridged) - Chapter 53",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 53-670332905.mp3",
        "duration": "1278.89",
        "chapter_id": "670332905",
        "post_id": "9813"
    }, {
        "track": 55,
        "name": "Warbreaker (Unabridged) - Chapter 54",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 54-670332890.mp3",
        "duration": "1658.56",
        "chapter_id": "670332890",
        "post_id": "9813"
    }, {
        "track": 56,
        "name": "Warbreaker (Unabridged) - Chapter 55",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 55-670332884.mp3",
        "duration": "1577.47",
        "chapter_id": "670332884",
        "post_id": "9813"
    }, {
        "track": 57,
        "name": "Warbreaker (Unabridged) - Chapter 56",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 56-670332881.mp3",
        "duration": "1382.22",
        "chapter_id": "670332881",
        "post_id": "9813"
    }, {
        "track": 58,
        "name": "Warbreaker (Unabridged) - Chapter 57",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 57-670332878.mp3",
        "duration": "1501.81",
        "chapter_id": "670332878",
        "post_id": "9813"
    }, {
        "track": 59,
        "name": "Warbreaker (Unabridged) - Chapter 58",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 58-670332869.mp3",
        "duration": "1256.08",
        "chapter_id": "670332869",
        "post_id": "9813"
    }, {
        "track": 60,
        "name": "Warbreaker (Unabridged) - Chapter 59",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 59-670332863.mp3",
        "duration": "1612.3",
        "chapter_id": "670332863",
        "post_id": "9813"
    }, {
        "track": 61,
        "name": "Warbreaker (Unabridged) - Chapter 60",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 60-670332857.mp3",
        "duration": "1866.26",
        "chapter_id": "670332857",
        "post_id": "9813"
    }, {
        "track": 62,
        "name": "Warbreaker (Unabridged) - Chapter 61",
        "chapter_link_dropbox": "warbreaker-audiobook-by-brandon-sanderson\/853729436\/Warbreaker (Unabridged) - Chapter 61-670332845.mp3",
        "duration": "781.055",
        "chapter_id": "670332845",
        "post_id": "9813"
    }
]'''


js = json.loads(js_str)
del js[0]
local_dir = r'C:\Users\User\Desktop\warbreaker-audiobook'
for f in js:
    url = base_url + f['chapter_link_dropbox']
    name = "{:02d}".format(int(f['track'])) + ' - ' + f['name'] + '.mp3'
    local_path = os.path.join(local_dir, name)
    # print(url, ' -> ', local_path)
    # url = url.replace(" ", "%20")
    os.system('wget "' + url + '" -O ' + '"' + name + '"')
    # urllib.request.urlretrieve(url, local_path)

a=5
