import os
import urllib.request
import json
import xmltodict
import xmljson
from lxml.etree import fromstring, tostring
from lxml2json import convert

shiurim = {
    65397: 'הלכה יומית',
    # 65154: 'טהרה',
    # 65131: 'שבת',
    # 11601: 'פרשת השבוע',
    # 65128: 'דיני השכמת בוקר',
    # 65129: 'הלכות תפילה',
    # 65130: 'הלכות ברכות',
    # 65132: 'מועדי השנה',
    # 65150: 'הלכות יום טוב',
    # 65151: 'שוע יורה דעה',
    # 65152: 'עין יעקב',
    # 65153: 'הלכות שונות',
    # 78719: 'פרקי אבות',
    # 35755: 'קריאת מגילה',
}

ravId = '2099'

root_dir = r'D:\שיעורים הרב יצחק יוסף'
try:
    os.mkdir(root_dir)
except:
    pass
main_url = 'https://www.kolhalashon.com/New/RSS/CreateRss.aspx?Lang=Hebrew&AudioOnly=True&Order=2&IsEdge=True&LangID=1&RavID=' + ravId + '&FID='
           # 'https://www.kolhalashon.com/New/RSS/CreateRss.aspx?Lang=Hebrew&FID=65021&AudioOnly=True&RavID=1810&Order=1&IsEdge=True&PageNum=1&LangID=1'

for fid, dir_to_save in shiurim.items():
    # fid = 65131
    # dir_to_save = 'שבת'
    root_url = main_url + str(fid) + '&PageNum='
    dir_to_save = os.path.join(root_dir, dir_to_save)

    def get_shiurim(url):
        xml_txt = urllib.request.urlopen(url).read().decode("utf-8")

        obj = xmltodict.parse(xml_txt.replace('&', '###'))
        print(json.dumps(obj))
        try:
            shiurim = obj['rss']['channel']['item']
        except:
            # print('error, item is None in url:', url)
            return []
        return shiurim

    idx = 1
    shiurim = get_shiurim(root_url + str(idx))
    shiur_idx = 1
    try:
        os.mkdir(dir_to_save)
    except:
        pass

    downloaded = list()

    while len(shiurim) > 0:
        for s in shiurim:
            # print(s)
            name = s['title']
            if name is None:
                name = ''
            name = name.replace('?', '')
            name = name.replace('"', "''")
            url = s['link']
            if url in downloaded:
                continue
            downloaded.append(url)
            name = "{:03d}".format(shiur_idx) + ' - ' + name
            f_path = os.path.join(dir_to_save, name + '.mp3')
            tmp_idx = 1
            while not os.path.exists(f_path):
                urllib.request.urlretrieve(url, f_path)

            print('name:', name, ' url:', url)
            shiur_idx += 1

        idx += 1
        shiurim = get_shiurim(root_url + str(idx))

    print('finish download ', dir_to_save)
print('finish download :-)')